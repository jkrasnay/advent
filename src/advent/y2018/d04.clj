(ns advent.y2018.d04
  (:require
    [advent.util :as util :refer [dbg dlet]]
    [advent.y2018.d04.data :as data]
    [clojure.string :as string]))

(def test-input "[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up")

#_ (re-matches #"\[([-0-9]+) (\d+):(\d+)\] (.*)" "[1518-11-05 00:03] Guard #99 begins shift")
#_ (->> (string/split-lines test-input) (map string/trim) sort)

; parses an individual string
(defn parse
  [s]
  (->> (re-matches #"\s*\[([-0-9]+) (\d+):(\d+)\] (.*)" s)
       rest
       vec))

(defn parse-input
 [input]
 (->> (string/split-lines input)
      (map parse)))

#_ (->> (string/split-lines test-input) (map parse))
; reduces a set of parsed strings down a sleep block: [guard-id sleep-minute wake-minute]
; sleep- and wake-minute are expressed as minutes since midnight

(defn mins-from-midnight
  [hour minute]
  (+ (* 60 (util/parse-int hour))
     (util/parse-int minute)))

(defn sleep-times
  [input]
  (loop [sleep-times []
         guard-id nil
         falls-asleep nil
         items (->> (string/split-lines input) (map string/trim) sort (map parse))]
    (if (seq items)
      (let [[date hour minute message] (first items)]
        (cond
          (string/starts-with? message "Guard")
          (let [[_ id] (re-matches #"Guard #(\d+).*" message)]
            (recur sleep-times id nil (rest items)))

          (= "falls asleep" message)
          (recur sleep-times guard-id (mins-from-midnight hour minute) (rest items))

          (= "wakes up" message)
          (recur (conj sleep-times [guard-id falls-asleep (mins-from-midnight hour minute)]) guard-id nil (rest items))))
      sleep-times)))

(defn total-time
  [times]
  (reduce (fn [sum [_ start end]] (+ sum (- end start))) 0 times))

(defn longest-sleeper
  [input]
  (->> (sleep-times input)
        (group-by first)
        (map (fn [[id times]] [id (total-time times)]))
        (sort-by second)
        last
        first))

#_ (longest-sleeper test-input)

(defn color-times
  [times]
  (loop [slots (vec (repeat 60 0))
         times times]
    (if (seq times)
      (let [[_ from to] (first times)]
        (recur (reduce #(update %1 %2 inc) slots (range from to)) (rest times)))
      slots)))

(defn answer1
  [input]
  (let [id (longest-sleeper input)
        times (filter (fn [[x _ _]] (= x id)) (sleep-times input))
        slot (->> (color-times times) (map-indexed vector) (apply max-key second) first)]
    (* (util/parse-int id) slot)))

#_ (map-indexed vector [1 3 2])
#_ (max)
#_ (answer1 test-input)
#_ (sleep-times data/input)
#_ (longest-sleeper data/input)
#_ (answer1 data/input)

#_ (re-matches #"Guard #(\d+).*" "Guard #99 begins shift")
#_ (def input test-input)
#_ (->> (sleep-times test-input)
        (group-by first)
        (map (fn [[id times]] [id (total-time times)])))

;; part 2

(defn answer2
  [input]
  (let [[id [slot _]] (->> (sleep-times input)
       (group-by first)
       (map (fn [[id times]] [id (->> (color-times times) (map-indexed vector) (apply max-key second))]))
       (apply max-key #(get-in % [1 1]))
       )]
    (* (util/parse-int id) slot)))

#_ (answer2 test-input)
#_ (answer2 data/input)
