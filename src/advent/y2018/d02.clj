(ns advent.y2018.d02
  (:require
    [advent.util :as util]
    [advent.y2018.d02.data :as data]
    [clojure.string :as string]))

#_ (->> data/input
        string/split-lines
        (map util/parse-int)
        (reduce +))

(defn first-repeat
  [xs]
  (loop [found? #{0}
         xs (->> xs
                 cycle
                 (reductions +))]
    (if (found? (first xs))
      (first xs)
      (recur (conj found? (first xs)) (rest xs)))))

#_ (first-repeat [1 -1])
#_ (first-repeat [3 3 4 -2 -4])
#_ (first-repeat [-6 3 8 5 -6])
#_ (first-repeat (->> data/input
                      string/split-lines
                      (map util/parse-int)))

(defn has-n
  [s n]
  (->> (frequencies s)
       vals
       (some #(= n %))))

(def input "abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab")

(defn count-with
  [ss n]
  (count (filter #(has-n % n) (string/split-lines ss))))

#_ (count-with input 2)
#_ (count-with input 3)

#_ (* (count-with data/input 2)
      (count-with data/input 3))


;; part 2

(defn diff-chars
  [s1 s2]
  (->> (map vector s1 s2)
       (filter (fn [[a b]] (not= a b)))
       count))

#_ (diff-chars "abc" "abd")
#_ (diff-chars "foo" "bar")

#_ (->> (for [a (string/split-lines data/input)
              b (string/split-lines data/input)]
          [(diff-chars a b) a b])
        ;(take 10)
        (filter #(= 1 (first %)))
        )

#_ (->> (map vector "umdryabviapkozistwcnihjqxg" "umdryabviapkozistwcnihjqxd")
        (filter (fn [[a b]] (= a b)))
        (map first)
        (apply str))
#_ (map vector "abc" "def")
