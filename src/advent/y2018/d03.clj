(ns advent.y2018.d03
  (:require
    [advent.util :as util :refer [dbg dlet]]
    [advent.y2018.d03.data :as data]
    [clojure.string :as string]))

; parse claim into x0 y0 x1 y1

#_ (re-matches #"#\d+ @ (\d+),(\d+): (\d+)x(\d+)" "#1 @ 704,926: 5x4")

(defn parse
  [s]
  (->> (re-matches #"\s*#\d+ @ (\d+),(\d+): (\d+)x(\d+)" s)
       rest
       (map util/parse-int)))

#_ (parse "#1 @ 704,926: 5x4")

(defn contains
  [[x y w h] [x0 y0]]
  (and (<= x x0 (dec (+ x w)))
       (<= y y0 (dec (+ y h)))))

(def test-input "#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2")

(defn contains-count
  [blocks point]
  (->> blocks
       (filter #(contains % point))
       count))

#_ (->> (string/split-lines test-input)
        (map parse))

(defn overlap-count
  [input]
  (let [blocks (->> (string/split-lines input)
                    (map parse))]
    (->> (for [x (range 1000)
               y (range 1000)]
           [x y])
         (map #(contains-count blocks %))
         (filter #(> % 1))
         count)))

#_ (overlap-count test-input)
#_ (overlap-count data/input)

(defn color-square
  [grid width [x y]]
  (update grid (+ x (* y width)) inc))

#_ (color-square (vec (repeat 9 0)) 3 [1 1])

(defn color-row
  [grid width [x y] w]
  (reduce (fn [g x1] (color-square g width [(+ x x1) y])) grid (range w)))

#_ (color-row (vec (repeat 9 0)) 3 [1 1] 2)

(defn color-squares
  [grid width [x y w h]]
  (reduce (fn [g y1] (color-row g width [x (+ y y1)] w)) grid (range h)))

#_ (color-squares (vec (repeat 9 0)) 3 [1 1 2 1])

(defn overlap-count-2
  [input width]
  (let [grid (vec (repeat (* width width) 0))
        blocks (->> (string/split-lines input)
                    (map parse))]
    (->> (reduce (fn [g block] (color-squares g width block)) grid blocks)
         (filter #(> % 1))
         count
         )))

#_ (def width 8)
#_ (def input test-input)
#_ (overlap-count-2 test-input 8)
#_ (overlap-count-2 data/input 1000)
#_ (color-square [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] 8 [1 3])


;; part 2


(defn parse-2
  [s]
  (->> (re-matches #"\s*#(\d+) @ (\d+),(\d+): (\d+)x(\d+)" s)
       rest
       (map util/parse-int)))

#_ (parse-2 "#1 @ 704,926: 5x4")

(defn range-overlaps
  [x0 w0 x1 w1]
  (and (< x1 (+ x0 w0))
       (> (+ x1 w1) x0)))

#_ (range-overlaps 2 2 1 1)

(defn blocks-overlap
  [[_ x0 y0 w0 h0] [_ x1 y1 w1 h1]]
  (and (range-overlaps x0 w0 x1 w1)
       (range-overlaps y0 h0 y1 h1)))

(defn overlaps
  [blocks block]
  (filter (fn [b] (and (not= b block)
                          (blocks-overlap b block))) blocks))

(defn non-overlapping
  [input]
  (let [blocks (->> (string/split-lines input)
                    (map parse-2))]
    (filter #(not (seq (overlaps blocks %))) blocks)
    ))

#_ (def test-blocks (->> (string/split-lines input) (map parse-2)))
#_ (def blocks test-blocks)
#_ (def block (first test-blocks))
#_ (blocks-overlap (first test-blocks) (last test-blocks))
#_ (overlaps test-blocks (last test-blocks))
#_ (non-overlapping test-input)
#_ (non-overlapping data/input)

