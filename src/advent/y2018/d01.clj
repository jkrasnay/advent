(ns advent.y2018.d01
  (:require
    [advent.util :as util]
    [advent.y2018.d01.data :as data]
    [clojure.string :as string]))

#_ (->> data/input
        string/split-lines
        (map util/parse-int)
        (reduce +))

(defn first-repeat
  [xs]
  (loop [found? #{0}
         xs (->> xs
                 cycle
                 (reductions +))]
    (if (found? (first xs))
      (first xs)
      (recur (conj found? (first xs)) (rest xs)))))

#_ (first-repeat [1 -1])
#_ (first-repeat [3 3 4 -2 -4])
#_ (first-repeat [-6 3 8 5 -6])
#_ (first-repeat (->> data/input
                      string/split-lines
                      (map util/parse-int)))
