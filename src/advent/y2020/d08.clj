(ns advent.y2020.d08
  (:require
    [clojure.set :refer [subset?]]
    [clojure.string :as string]
    [clojure.java.io :as io]))

(def test-data
  [

"nop +0"
"acc +1"
"jmp +4"
"acc +3"
"jmp -3"
"acc -99"
"acc +1"
"jmp -4"
"acc +6"

   ])

(defn parse-int
  ([x]
   (Integer/parseInt x))
  ([x default]
   (try (Integer/parseInt x)
        (catch Exception _
          default))))

(def real-data
  (->> (slurp (io/resource "advent/y2020/d08/input"))
       string/split-lines
       ))


(defn parse-opcode
  [s]
  (let [[op & args] (string/split s #"\s+")]
    (into [(keyword op)]
          (map parse-int args))))


(defn parse-data
  [data]
  (mapv parse-opcode data))


(defn step
  [state]
  (let [{:keys [acc pc code]} state
        [op & args] (nth code pc)]
    (condp = op
      :acc {:acc (+ acc (first args))
            :pc (inc pc)
            :code code}
      :jmp {:acc acc
            :pc (+ pc (first args))
            :code code}
      ; nop or unrecognized
      {:acc acc
       :pc (inc pc)
       :code code})))


(defn acc-at-loop
  [data]
  (loop [state {:acc 0
                :pc 0
                :code (parse-data data)}
         seen #{0}]
    (let [state (step state)]
      (if (contains? seen (:pc state))
        (:acc state)
        (recur state (conj seen (:pc state)))))))



(defn run
  "Runs the state machine until it either loops or terminates
  Returns a two-element vector [x state], where `x` is the
  termination condition, either :loop or :term
  "
  [code]
  (loop [state {:acc 0
                :pc 0
                :code code}
         seen #{0}]
    (let [state (step state)]
      (cond

        (contains? seen (:pc state))
        [:loop state]

        (>= (:pc state) (count code))
        [:term state]

        :else
        (recur state (conj seen (:pc state)))))))


(defn tweak-code
  [code i]
  (let [[op & args] (nth code i)]
    (condp = op
      :nop (assoc code i (into [:jmp] args))
      :jmp (assoc code i (into [:nop] args))
      code)))

(defn fix-code
  [data]
  (let [code (parse-data data)]
    (->> (range (count code))
         (map (fn [i]
                (run (tweak-code code i))))
         (filter #(= :term (first %)))
         first
         second
         :acc)))


#_(fix-code test-data)
#_(fix-code real-data)
#_(run (parse-data test-data))
#_(tweak-code (parse-data test-data) 0)
#_(tweak-code (parse-data test-data) 1)
#_(tweak-code (parse-data test-data) 2)

#_(acc-at-loop test-data)
#_(acc-at-loop real-data)

#_(let [state {:acc 0
               :pc 0
               :code (parse-data test-data)}]
    (->> (iterate step state)
         (take 10)))

#_(parse-opcode "nop +0")

#_(->> (parse-data test-data))
