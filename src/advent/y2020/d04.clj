(ns advent.y2020.d04
  (:require
    [clojure.set :refer [subset?]]
    [clojure.string :as string]
    [clojure.java.io :as io]))

(def test-data
  [

"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd"
"byr:1937 iyr:2017 cid:147 hgt:183cm"
""
"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884"
"hcl:#cfa07d byr:1929"
""
"hcl:#ae17e1 iyr:2013"
"eyr:2024"
"ecl:brn pid:760753108 byr:1931"
"hgt:179cm"
""
"hcl:#cfa07d eyr:2025 pid:166559648"
"iyr:2011 ecl:brn hgt:59in"

   ])

(defn parse-int
  ([x]
   (Integer/parseInt x))
  ([x default]
   (try (Integer/parseInt x)
        (catch Exception _
          default))))

(def real-data
  (->> (slurp (io/resource "advent/y2020/d04/input"))
       string/split-lines
       ))

(def tokens
  #{

"byr"
"iyr"
"eyr"
"hgt"
"hcl"
"ecl"
"pid"
;"cid"

    })

(defn valid?
  [m]
  (subset? tokens (set (keys m))))

(defn mapify
  [s]
  (->> (string/split s #"\s+")
       (map #(string/split % #":"))
       (into {})))

(defn valid-value?
  [k v]
  (condp = k
    "byr" (<= 1920 (parse-int v 0) 2002)
    "iyr" (<= 2010 (parse-int v 0) 2020)
    "eyr" (<= 2020 (parse-int v 0) 2030)
    "hgt" (let [[_ x unit] (re-matches #"(\d+)(in|cm)" v)]
            (condp = unit
              "cm" (<= 150 (parse-int x 0) 193)
              "in" (<= 59 (parse-int x 0) 76)
              false))
    "hcl" (re-matches #"#[0-9a-f]{6}" v)
    "ecl" (#{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"} v)
    "pid" (re-matches #"\d{9}" v)
    true
    ))

(defn day04a
  [data]
  (->> data
       (partition-by string/blank?)
       (map #(string/join " " %))
       (remove string/blank?)
       (map mapify)
       ;(map #(keys %))
       (filter valid?)
       count)
  )

(defn valid2?
  [m]
  (and (valid? m)
       (every? (fn [[k v]] (valid-value? k v)) m)))

(defn day04b
  [data]
  (->> data
       (partition-by string/blank?)
       (map #(string/join " " %))
       (remove string/blank?)
       (map mapify)
       ;(map #(keys %))
       (filter valid2?)
       count)
  )

#_(= #{:foo :bar} (set [:bar :foo]))
#_(day04a test-data)
#_(day04a real-data)

#_(day04b test-data)
#_(day04b real-data)
