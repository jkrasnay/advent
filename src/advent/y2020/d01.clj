(ns advent.y2020.d01
  (:require
    [clojure.string :as string]
    [clojure.java.io :as io]))

(defn parse-int
  [x]
  (Integer/parseInt x))

(def test-data
  [1721
   979
   366
   299
   675
   1456])

(def real-data
  (->> (slurp (io/resource "advent/y2020/d01/input"))
       string/split-lines
       (map parse-int)))

(defn day01b
  [data]
  (for [x data
        y data
        z data
        :when (< x y z)
        :when (= 2020 (+ x y z))]
    (* x y z)))

#_(day01a test-data)
#_(day01a real-data)
#_(day01b real-data)
