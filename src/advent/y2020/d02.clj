(ns advent.y2020.d02
  (:require
    [clojure.string :as string]
    [clojure.java.io :as io]))

(def test-data
  ["1-3 a: abcde"
   "1-3 b: cdefg"
   "2-9 c: ccccccccc"])

(defn parse-int
  [x]
  (Integer/parseInt x))

(def real-data
  (->> (slurp (io/resource "advent/y2020/d02/input"))
       string/split-lines))

(defn parse-rule
  [rule]
  (let [[min max c s] (string/split rule #"[- :]+")]
    {:min (parse-int min)
     :max (parse-int max)
     :c (first c)
     :s s}))

(defn valid?
  [{:keys [min max c s]}]
  (<= min (get (frequencies s) c 0) max))

(defn valid2?
  [{:keys [min max c s]}]
  (or (and (= c (nth s (dec min)))
           (not= c (nth s (dec max))))
      (and (not= c (nth s (dec min)))
           (= c (nth s (dec max)))))
  )

(->> test-data
     (map parse-rule)
     (map valid?))

(->> real-data
     (map parse-rule)
     (filter valid2?)
     count)

