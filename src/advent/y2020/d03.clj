(ns advent.y2020.d03
  (:require
    [clojure.string :as string]
    [clojure.java.io :as io]))

(def test-data
  ["..##......."
   "#...#...#.."
   ".#....#..#."
   "..#.#...#.#"
   ".#...##..#."
   "..#.##....."
   ".#.#.#....#"
   ".#........#"
   "#.##...#..."
   "#...##....#"
   ".#..#...#.#"])

(defn parse-int
  [x]
  (Integer/parseInt x))

(def real-data
  (->> (slurp (io/resource "advent/y2020/d03/input"))
       string/split-lines))

(defn tree-count
  [data dx dy]
  (let [row-size (count (first data))
        moves (->> (iterate (fn [[x y]]
                              [(mod (+ x dx) row-size) (+ y dy)])
                            [0 0])
                   (take-while (fn [[_ y]]
                                 (< y (count data)))))]
    (->> moves
         (map (fn [[x y]]
                (-> data
                    (nth y)
                    (nth x))))
         (filter #(= \# %))
         count)))

(defn day03a
  [data]
  (tree-count data 3 1))

#_(day03a test-data)
#_(day03a real-data)

(defn day03b
  [data]
  (* (tree-count data 1 1)
     (tree-count data 3 1)
     (tree-count data 5 1)
     (tree-count data 7 1)
     (tree-count data 1 2)))

#_(day03b test-data)
#_(day03b real-data)
