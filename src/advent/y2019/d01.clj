(ns advent.y2019.d01
  (:require
    [advent.util :as util]
    [clojure.java.io :as io]
    [clojure.string :as string]))

#_(slurp (io/resource "advent/y2019/d01/input"))

(fn [x] (* 2 x))
#(* 2 %)

(defn fuel
 [weight]
 (max 0 (- (Math/floor (/ weight 3)) 2)))

(defn fuel2
  [weight]
  (reduce + (take-while #(> % 0) (rest (iterate fuel weight)))))

(defn puzzle1
  [lines] (->> lines
               string/split-lines
               (map util/parse-int)
               (map fuel2)
               (reduce +)
               ))

#_(puzzle1 "1969")
#_(puzzle1 "100756")
#_(puzzle1 (slurp (io/resource "advent/y2019/d01/input")))
#_(a (b (c 27)))
#_(-> 27 c b a)
#_(reduce f init seq)

#_(reduce + (take-while #(> % 0) (rest (iterate fuel 1969))))
