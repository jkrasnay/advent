(ns advent.y2019.d03
  (:require
    [advent.util :as util]
    [clojure.java.io :as io]
  [clojure.set :refer [intersection]]
    [clojure.string :as string]))

(def direction
  {
   "L" [-1 0]
   "R" [1 0]
   "U" [0 1]
   "D" [0 -1]
   })

(defn +v
  [v1 v2]
  (mapv + v1 v2))

#_(+v [1 2] [3 4])

(defn *v
  [k v]
  (mapv #(* k %) v))

(defn abs
  [^Integer i]
  (Math/abs i))

(defn manh
  [v]
  (->> v
       (map abs)
       (reduce +)))

#_(manh [1 -3])

(defn parse-run
  "Splits a string like R25 to a run like [[0 1] 25]"
  [s]
  [(direction (subs s 0 1)) (util/parse-int (subs s 1))])

#_(split "R1")

(defn parse-line
  [s]
  (->> (string/split s #",")
       (mapv parse-run)))

(defn parse
  [s]
  (->> (string/split s #"\n")
       (mapv parse-line)))

#_(parse "U3\nR3\n")
#_(parse "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n")
#_(parse (slurp (io/resource "advent/y2019/d02/input")))




(defn points-run
  "Returns a seq of the points crossed given a starting point and a run."
  [start [dir len]]
  (->> (range len)
       (map #(+v start (*v (inc %) dir))))
  )

#_(points-run [10 20] [[0 1] 5])

(defn points-wire
  [wire]
  (reduce (fn [acc run]
            (into acc (points-run (last acc) run)))
          [[0 0]]
          wire))

#_(points-wire [[[1 0] 5] [[0 1] 4]])

(defn intersections
  [wires]
  (->> (map points-wire wires)
       (map set)
       (apply intersection)))

#_(->> (parse "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n")
       intersections
       (map manh)
       (remove #{0})
       (apply min))

(defn closest-intersection
  [wires]
  (->> wires
       intersections
       (map manh)
       (remove #{0})
       (apply min)))

#_(closest-intersection (parse "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n"))
#_(closest-intersection (parse "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7\n"))
#_(closest-intersection (parse (slurp (io/resource "advent/y2019/d03/input"))))

;-- part 2

(defn point->delay
  "For a seq of points, returns a map of the point to the shortest delay taken to get to the point."
  [points]
  (->> points
       (map-indexed vector)
       (reduce (fn [m [i p]]
                 (if (get m p)
                   m
                   (assoc m p i)))
               {})))

#_(-> (parse-line "R75,D30,R83,U83,L12,D49,R71,U7,L72")
      points-wire
      point->delay
      )

(defn point->delay2
  "Combine a point->delay for multiple wires by summing the delays."
  [wires]
  (->> wires
       (map point->delay)
       (apply merge-with)))

#_(point->delay2 (parse "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n"))

(defn least-delay-intersection
  [wires]
  (->> wires
       intersections
       (map point->delay2)
       ))

#_(least-delay-intersection (parse "R8,U5,L5,D3\nU7,R6,D4,L4\n"))
#_(least-delay-intersection (parse "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n"))
