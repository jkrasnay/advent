(ns advent.y2019.d02
  (:require
    [advent.util :as util]
    [clojure.java.io :as io]
    [clojure.string :as string]))

(defn step
  [[i v]]
  (let [[op x y d] (drop i v)]
    (condp = op
      1 [(+ i 4) (assoc v d (+ (get v x) (get v y)))]
      2 [(+ i 4) (assoc v d (* (get v x) (get v y)))]
      (throw (ex-info (str "unexpected opcode " op " at position " i) {}))
      )))

(defn exec
  [v]
  (-> (drop-while (fn [[i v]] (not= 99 (get v i)))
                  (iterate step [0 v]))
      first second first
      ))

(defn fix-exec
  [v noun verb]
  (-> v
    (assoc 1 noun)
    (assoc 2 verb)
    exec))

(defn parse
  [s]
  (as-> s $
    (string/replace $ "\n" "")
    (string/split $ #",")
    (mapv util/parse-int $)))

#_(parse "1,9,10,3,2,3,11,0,99,30,40,50\n")
#_(exec "1,9,10,3,2,3,11,0,99,30,40,50")
#_(exec "2,4,4,5,99,0")

#_(parse (slurp (io/resource "advent/y2019/d02/input")))
#_(-> (slurp (io/resource "advent/y2019/d02/input"))
      parse
      (fix-exec 12 2))

#_(-> (slurp (io/resource "advent/y2019/d02/input"))
      (string/replace "\n" "")
      (string/split #","))
#_(exec "1,9,10,3,2,3,11,0,99,30,40,50")
#_(let [v (->> (string/split "1,9,10,3,2,3,11,0,99,30,40,50" #",")
       (mapv util/parse-int)
       ;exec
       )]
    ;(take 3 (iterate step [0 v]))
    (exec v)
    )

#_(let [v (parse (slurp (io/resource "advent/y2019/d02/input")))
        [noun verb] (first (->> (for [noun (range 100)
                                      verb (range 100)]
                                  [noun verb])
                                (drop-while (fn [[noun verb]]
                                              (not= 19690720 (fix-exec v noun verb))))))]
    (+ (* 100 noun) verb))

#_(let [[a b c :as op] (range 10)] op)
