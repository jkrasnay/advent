(ns advent.util)

(defn parse-int
  [s]
  (Integer/parseInt s))

(defn log
  [form value]
  (println "dbg:" form "=" value))

(defmacro dbg [x] `(let [x# ~x] (log '~x x#) x#))

; See http://brownsofa.org/blog/2014/08/03/debugging-in-clojure-tools/
(defmacro dlet [bindings & body]
  `(let [~@(mapcat (fn [[n v]]
                     (if (or (vector? n) (map? n))
                       [n v]
                       [n v '_ `(println (name '~n) ":" ~v)]))
                   (partition 2 bindings))]
     ~@body))

